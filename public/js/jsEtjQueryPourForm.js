// On attend que la page soit chargée 
jQuery(document).ready(function () {
    // On cache la zone de texte
    jQuery('#toggle1').hide();
    jQuery('#toggle2').hide();
    jQuery('#toggle3').hide();
    jQuery('#toggle4').hide();
    // toggle() lorsque le lien avec l'ID #toggler est cliqué
    jQuery('a#toggler1').click(function () {
        jQuery('#toggle1').toggle(400);
        return false;
    });
    jQuery('a#toggler2').click(function () {
        jQuery('#toggle2').toggle(400);
        return false;
    });
    jQuery('a#toggler3').click(function () {
        jQuery('#toggle3').toggle(400);
        return false;
    });
    jQuery('a#toggler4').click(function () {
        jQuery('#toggle4').toggle(400);
        return false;
    });
    jQuery('a#toggler5').click(function () {
        jQuery('#toggle5').toggle(400);
        return false;
    });


    // jongler entre le formulaire au jour et le formulaire au mois
    if(window.location.hash =='#jour'){
        jQuery('#formMois').hide();
        $("#selectJour").prop("checked", true)
    }else{
        jQuery('#formJour').hide();
        $("#selectMois").prop("checked", true)
    }

    jQuery('#selectMois').click(function () {
        jQuery('#formJour').hide();
        jQuery('#formMois').show();
    });

    jQuery('#selectJour').click(function () {
        jQuery('#formMois').hide();
        jQuery('#formJour').show();
    });
});
