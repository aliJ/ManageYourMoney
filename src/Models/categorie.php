<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class categorie extends Eloquent
{	
	protected $table = 'categorie';
	protected $filable = 'nom';

    /**
     * Fonction qui permet de récupérer les sous catégories d'une catégorie
     * @return mixed
     */
    public function getSousCategories(){
        return sous_categorie::where("id_categorie","=",$this->id_categorie)->get();
    }
}
?>