<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class budget extends Eloquent
{
	
	protected $table = 'budget';
    protected $fillable = [
        'montant',
        'id_user', 
        'id_sous_categorie',
        'date'
        ];

    /**
     * Convertir un chiffre en mois français
     * @param int $mois
     * @return string
     */
    static public function moisEnFr($mois){
        $mois_fr = Array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août",
            "Septembre", "Octobre", "Novembre", "Décembre");
        return isset($mois_fr[(int)$mois])? $mois_fr[(int)$mois] : '';
    }

    public function sousCategorie()
    {
        return $this->belongsTo('sous_categorie', 'id_sous_categorie', 'id_sous_categorie');
    }
}
