<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class sous_categorie extends Eloquent
{
	protected $table = 'sous_categorie';
	protected $filable = 'nom_ss_cat';
    protected $primaryKey = 'id_sous_categorie';

    public function getBudget($mois, $annee, $id_user){
        /** @var budget $budget */
        $budget = budget::whereYear('created_at', '=', $annee)
            ->whereMonth('created_at', '=', $mois)
            ->where('id_sous_categorie', '=', $this->id_sous_categorie)
            ->where('id_user', '=', $id_user)
            ->first();
        /**
         * Si un montant n'existe pas dans la base, budget est null donc il faut le créer
         */
        if ($budget==NULL){
            $budget = new budget();
        }
        return $budget;
    }
}
?>