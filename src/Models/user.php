<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class user extends Eloquent
{
	protected $table = 'user';

	protected $fillable = [
		'nom',
		'prenom',
		'pseudo',
		'email',
		'password',
	];

}