<?php
//$ php composer.phar create-project slim/slim-skeleton [my-app-name] --> pour créer un projet
//$ cd ../../wamp/www/ManageYourMoneyV2; php -S 0.0.0.0:8282 -t public public/index.php --> dans la ligne de commande

// Routes

//PAGE CONSEILS
$app->get('/conseils', function ($request, $response, $args) {
    // Render index view
    $this->renderer->render($response, 'header.phtml', $args);
    $this->renderer->render($response, 'conseils.phtml', $args);
    $this->renderer->render($response, 'footer.phtml', $args);
    return $response;
});


//RESULTATS
$app->get('/resultats[/{annee}/{mois}]', function ($request, $response, $args) {
    // Render index view
    if(isset($_SESSION['user'])){
        $this->db; //on se connecte à la base de données
        $categories = categorie::all();
        $args['categories']= $categories;
        $this->renderer->render($response, 'header.phtml', $args);
        $this->renderer->render($response, 'resultats.phtml', $args);
        $this->renderer->render($response, 'footer.phtml', $args);
        $this->db->getConnection()->disconnect(); // déconnexion explicite à cause des limitations de l'hébergeur
        return $response;
    }else{
        return $response->withRedirect('/connexion');
    }
});


//POST BUDGET : juste pour les traitements du formulaire au mois
$app->post('/budget', function ($request, $response, $args) use ($app) {
    /** @var $this Slim\Container */
    // Render index view
    if(isset($_SESSION['user'])){
        $this->db; //on se connecte à la base de données
        //traite les données
	    require('phpRoutes/phpForm.php');
        $this->db->getConnection()->disconnect(); // déconnexion explicite à cause des limitations de l'hébergeur
        //redirige vers le get
        return $response->withRedirect('/budget#mois');
    }else{
        return $response->withRedirect('/connexion');
    }
	
});
//POST BUDGET : juste pour les traitements du formulaire au jour
$app->post('/budgetJourPost', function ($request, $response, $args) use ($app) {
    /** @var $this Slim\Container */
    // Render index view
    if(isset($_SESSION['user'])){
        $this->db; //on se connecte à la base de données
        //traite les données
        require('phpRoutes/phpForm.php');
        $this->db->getConnection()->disconnect(); // déconnexion explicite à cause des limitations de l'hébergeur
        //redirige vers le get
        return $response->withRedirect('/budget#jour');
    }else{
        return $response->withRedirect('/connexion');
    }

});
//GET BUDGET: pour afficher le formulaire
$app->get('/budget', function ($request, $response, $args) {
    // Render index view
	
    
    if(isset($_SESSION['user'])){
        $this->db; //on se connecte à la base de données
        $categories = categorie::all();
        $messages = $this->flash->getMessages();
        $this->renderer->render($response, 'header.phtml', $args);
        $this->renderer->render($response, 'budget.phtml', ['categories' => $categories, 'messages' => $messages]);
        $this->renderer->render($response, 'footer.phtml', $args);
        $this->db->getConnection()->disconnect(); // déconnexion explicite à cause des limitations de l'hébergeur
        return $response;
    }else{
        return $response->withRedirect('/connexion');
    }
    
});

//PAGE OBJECTIF
$app->get('/objectif', function ($request, $response, $args) {
    // Render index view
    //$this->db; //on se connecte à la base de données
    $this->renderer->render($response, 'header.phtml', $args);
    $this->renderer->render($response, 'objectif.phtml', $args);
    $this->renderer->render($response, 'footer.phtml', $args);
    return $response;
});

//PAGE D'ACCUEIL
$app->get('/', function ($request, $response, $args) {
    // Sample log message
    $messages = $this->flash->getMessages();
    $this->logger->info("Slim-Skeleton '/' route");
    $this->renderer->render($response, 'header.phtml', $args);
    $this->renderer->render($response, 'index.phtml', ['messages' => $messages]);
    $this->renderer->render($response, 'footer.phtml', $args);
    // Render index view
    return $response;
});

//GET INSCRIPTION
$app->get('/inscription', function ($request, $response, $args) {
    // Render index view
    $messages = $this->flash->getMessages();
    $this->renderer->render($response, 'header.phtml', $args);
    $this->renderer->render($response, 'inscription.phtml', ['messages' => $messages]);
    $this->renderer->render($response, 'footer.phtml', $args);
    return $response;
});

//POST INSCRIPTION:
$app->post('/inscription', function ($request, $response, $args) use ($app) {
    /** @var $this Slim\Container */
    // Render index view
    $this->db; //on se connecte à la base de données
    //traite les données
    require('phpRoutes/inscription.php');
    $this->db->getConnection()->disconnect(); // déconnexion explicite à cause des limitations de l'hébergeur
    //redirige vers le get
    if($validate){
        return $response->withRedirect('/');
    }
    return $response->withRedirect('/inscription');
});

//GET CONNEXION
$app->get('/connexion', function ($request, $response, $args) {
    // Render index view
    $messages = $this->flash->getMessages();  
    $this->renderer->render($response, 'header.phtml', $args);
    $this->renderer->render($response, 'connexion.phtml', ['messages' => $messages]);
    $this->renderer->render($response, 'footer.phtml', $args);
    return $response;
});


//POST CONNEXION:
$app->post('/connexion', function ($request, $response, $args) use ($app) {
    /** @var $this Slim\Container */
    // Render index view
    $this->db; //on se connecte à la base de données
    //traite les données
    require('phpRoutes/connexion.php');
    $this->db->getConnection()->disconnect(); // déconnexion explicite à cause des limitations de l'hébergeur
    //redirige vers le get
    if($validate == true){
        return $response->withRedirect('/');
    }
    return $response->withRedirect('/connexion'); 
    
   
});

//GET DECONNEXION
$app->get('/deconnexion', function ($request, $response, $args) {
    unset($_SESSION['user']); 
    unset($_SESSION['pseudo']); 
    return $response->withRedirect('/');
});

//GET SUPREESSION
$app->get('/supression', function ($request, $response, $args) use ($app) {
    $this->db;
    $messages = $this->flash->getMessages();
    user::destroy($_SESSION['user']);
    unset($_SESSION['user']); 
    unset($_SESSION['pseudo']);
    $this->db->getConnection()->disconnect(); 
    return $response->withRedirect('/');
    $flash->addMessage("success", "Votre compte à bien été supprimé !");
});



//GET CHANGE PASSWORD
$app->get('/change_password', function ($request, $response, $args) {
    // Render index view

    $messages = $this->flash->getMessages();  
    $this->renderer->render($response, 'header.phtml', $args);
    $this->renderer->render($response, 'change_password.phtml', ['messages' => $messages]);
    $this->renderer->render($response, 'footer.phtml', $args);
    return $response;
});

//POST CHANGE PASSWORD
$app->post('/change_password', function ($request, $response, $args) use ($app){
    // Render index view
    $this->db;
    require('phpRoutes/change_password.php'); 
    $this->db->getConnection()->disconnect();
    if($validate){
        return $response->withRedirect('/');
    }
    return $response->withRedirect('/change_password');
});