<?php

$flash = $this->flash;


$email = $_POST['email'];
$password = $_POST['password'];
$validate = false;


$user = user::where('email', $email)->first();

if (!$user) {
	$flash->addMessage("error", "Utilisateur non valide!");
	
}else{

	if(password_verify($password, $user->password)){
		$_SESSION['user'] = $user->id;
		$_SESSION['pseudo'] = $user->pseudo; 
		$flash->addMessage("success", "Vous êtes connecté !");
		$validate = true;

	}else{
		$flash->addMessage("error", "Mauvais mot de passe !"); 
	}
}


		
            