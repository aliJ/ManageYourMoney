<?php

$flash = $this->flash;
$validate = false; 
$old_password = $_POST['password_old'];

$user = user::find($_SESSION['user']); 

if(password_verify($old_password, $user->password)){
    $new_password = $_POST['password']; 
    $comfirm_password = $_POST['confirmPassword']; 

	

    	if($new_password == $comfirm_password){
        	$user->password = password_hash($new_password, PASSWORD_DEFAULT, ['cost' => 10]); 
        	$user->save(); 
        	$flash->addMessage("success", "Votre mot de passe à bien été modifié !");
        	$validate = true;

        	}else{
		$flash->addMessage("error", "Confirmation mot de passe échouée !");
		}

	}else{

    $flash->addMessage("error", "Ce n'est pas votre mot de passe ! ");
    	
}

    
