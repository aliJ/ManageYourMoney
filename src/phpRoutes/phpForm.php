<?php
    //Je récupère le mois et l'année actuelle
    $moisEnCours = date("m");
    $anneeEnCours = date("Y");
    $dateEnCours = date("Y-m");
    /** @var $this Slim\Container */
    /** @var $flash \Slim\Flash\Messages */
    $flash = $this->flash;

	if(isset($_POST['btn-init-categorie'])) //si on a appuyé sur le bouton
    {
        $arrayCat = $_POST['cat'];
        $nbSaved = 0;
        foreach ($arrayCat as $idCategorie => $arraySousCategories) {
            foreach ($arraySousCategories as $idSousCategorie => $montant) {
                if (is_numeric($montant)) {
                    $sousCategory = sous_categorie::find($idSousCategorie);
                    /** @var budget $budget */
                    $budget = $sousCategory->getBudget($moisEnCours, $anneeEnCours, $_SESSION['user']);
                    $budget->id_user = $_SESSION['user'];
                    $budget->montant = $montant;
                    $budget->id_sous_categorie = $idSousCategorie;
                    $budget->date = $dateEnCours;
                    $budget->updated_at = date('Y-m-d H:i:s');
                    $budget->save();
                    $nbSaved++;
                }
            }
        }
        if ($nbSaved) {
            $flash->addMessage("success", "Vos données ont bien été enregistrées !");
        } else {
            $flash->addMessage("error", "Oooops ! Êtes-vous sûr d'avoir renseigné une valeur numérique ..?");
        }
    }

    if(isset($_POST['submit-day'])) //si on a appuyé sur le bouton
    {
        $montant = $_POST['montantJour'];
        $idSousCategorie = $_POST['idCatSelect'];

        if (is_numeric($montant)) {
            $sousCategory = sous_categorie::find($idSousCategorie);
            /** @var budget $budget */
            $budget = $sousCategory->getBudget($moisEnCours, $anneeEnCours, $_SESSION['user']);
            $budget->id_user = $_SESSION['user'];
            $budget->montant += $montant;
            $budget->id_sous_categorie = $idSousCategorie;
            $budget->date = $dateEnCours;
            $budget->updated_at = date('Y-m-d H:i:s');
            $budget->save();

            $flash->addMessage("success", "Le montant total pour ".$sousCategory->nom_ss_cat." est maintenant de ".$budget->montant."€" );
        } else {
            $flash->addMessage("error", "Oooops ! Êtes-vous sûr d'avoir renseigné une valeur numérique ..?");
        }
    }


//			<div id="snackbar" style="background-color:#DC4027;">Oooops ! Êtes-vous sûr d';echo"'avoir";echo' renseigné une valeur numérique ..?</div>
//			<div id="snackbar">Vos données ont bien été enregistrées !</div>
//          <script>
//			var x = document.getElementById("snackbar")
//				x.className = "show";
//				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
//			</script>


	//PETIT FORMULAIRE	
//	if(isset($_POST['btn-ajouter'])) //si on a appuyé sur le bouton
//	{
//		$umontant = strip_tags($_POST['montant']); //on récupère les entrées du formulaire
//		$usous_categorie = strip_tags($_POST['sous-catégorie']);
//		if(is_numeric($umontant)==true){
//
//			//On récupère l'id_categorie de la sous catégorie
//			$sous_categories = sous_categorie::where('id_sous_categorie', '=', $usous_categorie)->first();
//			$idAtrouver = $sous_categories->id_categorie;
//
//			//Je vérifie que j'ai déjà entré quelque chose ce mois-ci
//			$budgets = budget::where('id_categorie','=', $idAtrouver)->get();
//			$flag=0;
//			foreach ($budgets as $budget)
//				{
//					 if($budget->date  == $dateEnCours){
//						 $flag=1;
//					 }
//				}
//			if($flag == 1){ //J'ai déjà des ressources d'enregistrées, il faut donc que tu les modifies !
//				//Je vérifie s'il existe une ligne dans mon tableau qui a pour date de création
//				//le même mois et la même année qu'aujourd'hui avec l'id_catégorie que je souhaite avoir
//				$budgets = budget::whereYear('created_at','=', $annéeEnCours)->whereMonth('created_at','=', $moisEnCours)->where('id_categorie','=', $idAtrouver)->first();
//				$budgets->montant = $budgets->montant + $umontant;
//				$budgets->save();
//			}
//			else{//J'ai pas ça en stock, tu peux en créer un !
//				$budget = new budget();
//				$budget->montant = $umontant;
//				$budget->id_categorie = $idAtrouver;
//				$budget->date = $dateEnCours;
//				$budget->save();
//			}
//			echo'
//			</br>
//			<div class="col-lg-3"></div>
//			<div class="col-lg-9">
//			<center><a type="button" class="btn btn-info" href="/resultats">Accédez à vos résultats</a></center></div>
//			<div id="snackbar">C'; echo'est fait ! Vos données ont bien été enregistrées.</div>
//			<script>
//			var x = document.getElementById("snackbar")
//			x.className = "show";
//			setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
//			</script>
//			';
//		}
//		else{
//			echo'
//			<div id="snackbar" style="background-color:#DC4027;">Oooops ! Êtes-vous sûr d';echo"'avoir";echo' renseigné une valeur numérique ..?</div>
//			<script>
//			var x = document.getElementById("snackbar")
//				x.className = "show";
//				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
//			</script>
//			';
//		}
//	}
